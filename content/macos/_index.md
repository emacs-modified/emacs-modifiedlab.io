---
title: Emacs Modified for macOS
description: GNU Emacs. Ready for R and LaTeX.
section: macos
---

**Emacs Modified for macOS** is a distribution of GNU Emacs 30.1 bundled with the following packages:

### From ELPA

- [ESS](https://ess.r-project.org) {{< version ess-macos >}};
- [AUCTeX](https://www.gnu.org/software/auctex/) {{< version auctex-macos >}};

### From MELPA-stable

- [polymode](https://polymode.github.io) {{< version polymode-macos >}} and the polymodes poly-R, poly-noweb, poly-markdown;
- [markdown-mode](https://jblevins.org/projects/markdown-mode/) {{< version markdown-macos >}};
- [exec-path-from-shell](https://github.com/purcell/exec-path-from-shell) {{< version execpath-macos >}} to import the user's environment at Emacs startup;

### From other sources

- [Tabbar](https://github.com/dholm/tabbar) {{< version tabbar-windows >}};
- dictionaries for the [Hunspell](https://hunspell.github.io) spell checker (optional; see below for details);
- `default.el` and `site-start.el`, configuration files to make everything work.

The current version is **{{< version version-macos >}}**.
{{< see-release-notes news-macos >}}

## System requirements

Mac OS X 10.4 or later.

## Installation

Open the disk image and copy Emacs in the `Applications` folder or any other folder.

## Interaction with the Emacs package system

Many of the bundled packages are installed from archives using the Emacs package system. Therefore, by default the system is active and the packages are initialized. Because we have done this for you, your init file should **not** contain the expression `(package-initialize)`.

You may install additional packages locally, but you should refrain from updating the bundled packages (marked `external` in `M-x list-packages`). To install packages from the [MELPA](https://melpa.org/) archive, add the following lines to your init file:

```emacs-lisp
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
```

## Spell checking and dictionaries

Spell checking inside Emacs on macOS requires an external checker. I recommend to install [Hunspell](https://hunspell.github.io) using [Homebrew](https://brew.sh).

The Hunspell installation does not include any dictionaries. Therefore, this distributions of Emacs ships with the following [Libre Office dictionaries](https://extensions.libreoffice.org/extensions?getCategories=Dictionary&getCompatibility=any) suitable for use with Hunspell:
[English](https://extensions.libreoffice.org/extensions/english-dictionaries/) (version {{< version dict-en-macos >}});
[French](https://extensions.libreoffice.org/extensions/dictionnaires-francais/) (version {{< version dict-fr-macos >}});
[German](https://extensions.libreoffice.org/extensions/german-de-de-frami-dictionaries) (version {{< version dict-de-macos >}});
[Spanish](https://extensions.libreoffice.org/extensions/spanish-dictionaries) (version {{< version dict-es-macos >}}).

To make use of the dictionaries, copy the files in the `Dictionaries` directory of the disk image to `~/Library/Spelling`. If needed, create a symbolic link named after your `LANG` environment variable to the corresponding dictionary and affix files. For example, if `LANG` is set to `fr_CA.URF-8`, do from the command line

```shell-session
$ cd ~/Library/Spelling
$ ln -s fr-classique.dic fr_CA.dic
$ ln -s fr-classique.aff fr_CA.aff
```

Finally, if you have a Mac with an Apple Silicon CPU (M1 and above), add the following lines to your ~/.emacs file:

```emacs-lisp
(setq-default ispell-program-name "/opt/homebrew/bin/hunspell")
(setq ispell-really-hunspell t)
```

For an Intel CPU, use instead:

```emacs-lisp
(setq-default ispell-program-name "/usr/local/bin/hunspell")
(setq ispell-really-hunspell t)
```

Spell checking should now work with `M-x ispell`.


<!-- Local Variables: -->
<!-- eval: (auto-fill-mode -1) -->
<!-- eval: (visual-line-mode) -->
<!-- End: -->
