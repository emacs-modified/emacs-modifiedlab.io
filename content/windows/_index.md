---
title: Emacs Modified for Windows
description: GNU Emacs. Ready for R and LaTeX.
---

**Emacs Modified for Windows** is a distribution of GNU Emacs 30.1 bundled with the following packages:

### From ELPA

- [ESS](https://ess.r-project.org) {{< version ess-windows >}};
- [AUCTeX](https://www.gnu.org/software/auctex/) {{< version auctex-windows >}};

### From MELPA-stable

- [polymode](https://polymode.github.io) {{< version polymode-windows >}} and the polymodes poly-R, poly-noweb, poly-markdown;
- [markdown-mode](https://jblevins.org/projects/markdown-mode/) {{< version markdown-windows >}};

### From other sources

- [Tabbar](https://github.com/dholm/tabbar) {{< version tabbar-windows >}};
- [Hunspell](https://hunspell.github.io/) {{< version hunspell-windows >}}, a spell checker well integrated with Emacs, and some popular dictionaries (see below for details);
- `default.el` and `site-start.el`, configuration files to make everything work.

The current version is **{{< version version-windows >}}**.
{{< see-release-notes news-windows >}}

## System requirements

64-bit version of Microsoft Windows.

## Installation

Start the setup wizard and follow the instructions on screen.

## Interaction with the Emacs package system

Many of the bundled packages are installed from archives using the Emacs package system. Therefore, by default the system is active and the packages are initialized. Because we have done this for you, your init file should **not** contain the expression `(package-initialize)`.

You may install additional packages locally, but you should refrain from updating the bundled packages (marked `external` in `M-x list-packages`). To install packages from the [MELPA](https://melpa.org/) archive, add the following lines to your init file:

```emacs-lisp
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
```

## Included libraries

Emacs Modified for Windows is based on the official GNU release of Emacs with the optional dependency libraries that enable support for the following:

- displaying inline images of many types (PNG, JPEG, GIF, TIFF, SVG);
- SSL/TLS secure network communications (HTTPS, IMAPS, etc.);
- HTML and XML parsing (necessary for the built-in EWW browser);
- built-in decompression of compressed text.

## Images and preview-latex mode

This version of Emacs bundles the libraries needed to display images in formats XPM, PNG, JPEG, TIFF, GIF and SVG supported on Windows since Emacs version 22.1. Among other things, this means that the toolbar displays in color, that the ESS toolbar displays correctly and that the preview-latex mode of [AUCTeX](https://www.gnu.org/software/auctex/) works to its full extent. However, the latter requires to separately install [Ghostscript](https://www.ghostscript.com/) and to make sure that the file `gswin32c.exe` or `gswin64c.exe` is in a folder along the `PATH` environment variable.

The previous comment also applies to the image conversion tool [ImageMagick](https://www.imagemagick.org/) that may be required by Org. If you need the tool, install it and make sure its location is along the `PATH`.

## Spell checking and dictionaries

This distribution ships with [Hunspell](https://hunspell.github.io) for spell checking inside Emacs, along with the following [Libre Office dictionaries](https://extensions.libreoffice.org/extensions?getCategories=Dictionary&getCompatibility=any) suitable for use with Hunspell:
[English](https://extensions.libreoffice.org/extensions/english-dictionaries/) (version {{< version dict-en-windows >}});
[French](https://extensions.libreoffice.org/extensions/dictionnaires-francais/) (version {{< version dict-fr-windows >}});
[German](https://extensions.libreoffice.org/extensions/german-de-de-frami-dictionaries) (version {{< version dict-de-windows >}});
[Spanish](https://extensions.libreoffice.org/extensions/spanish-dictionaries) (version {{< version dict-es-windows >}}).

The default dictionary for Hunspell is American English.

## Unix applications

Emacs sometimes uses external applications that are standard on Unix but not available on Windows (for example: `diff`, `gzip`). When needed, install the applications from the [ezwinports](https://sourceforge.net/projects/ezwinports/) project. To make sure Emacs can find the applications, include the folder where they are installed to the `PATH` environment variable. *(With thanks to Laurent Pantera for the hint.)*

<!-- Local Variables: -->
<!-- eval: (auto-fill-mode -1) -->
<!-- eval: (visual-line-mode) -->
<!-- End: -->
