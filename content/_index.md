---
title: Emacs Modified for (macOS|Windows)
description: GNU Emacs. Ready for R and LaTeX.
---

**[Emacs Modified for macOS](macos)** and **[Emacs Modified for Windows](windows)** are distributions of [GNU Emacs](https://www.gnu.org/software/emacs/) bundled with a few select packages for R developers and LaTeX users, most notably [ESS](https://ess.r-project.org) and [AUCTeX](https://www.gnu.org/software/auctex/).

## Philosophy

The distributions are based on the official sources of GNU Emacs. Other than a few selected packages and some minor configuration, they are stock distributions of Emacs with a similar look and feel not only between them, but also with Emacs on other platforms.

The project started with the Windows version (probably around 2002 or so), when I wanted to provide my students with an Emacs fully setup for R programming and that would be easy to install --- that is, distributed with an installation wizard.

The macOS distribution is based on [Emacs for Mac OS X](https://emacsformacosx.com) by David Caldwell. Those looking for a more Mac-like version of Emacs may consider [Aquamacs](http://aquamacs.org), but be prepared to have Aquamacs play a lot with frames and fonts behind you. Furthermore, these days the development of Aquamacs is much behind GNU Emacs.

## Author

[Vincent Goulet](https://vigou3.gitlab.io), École d'actuariat, Université Laval

<!-- Local Variables: -->
<!-- eval: (auto-fill-mode -1) -->
<!-- eval: (visual-line-mode) -->
<!-- End: -->
