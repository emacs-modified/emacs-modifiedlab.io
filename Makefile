### -*-Makefile-*- to update the web site of Emacs Modified for
### (macOS|Windows)
##
## Copyright (C) 2023 Vincent Goulet
##
## Author: Vincent Goulet
##
## This file is part of Emacs Modified for (macOS|Windows) web site
## https://gitlab.com/emacs-modified/emacs-modified.gitlab.io

## List of targets (one per supported OS)
TARGETS = macos windows

## Files
CONFIG = hugo.toml

## GitLab repository and authentication
GROUPNAME = emacs-modified
REPOSSTEM = ${GROUPNAME}
HEAD = master
APIURL = https://gitlab.com/api/v4/projects/${GROUPNAME}%2F${REPOSSTEM}
OAUTHTOKEN = $(shell cat ~/.gitlab/token)

all: $(addprefix config-,${TARGETS}) commit

## Fetching of Makeconf files
Makeconf-%:
	$(eval OS=$(word 2,$(subst -, ,$@)))
	curl --header "PRIVATE-TOKEN: $${OAUTHTOKEN}" \
	     --silent \
	     "${APIURL}-${OS}/repository/files/Makeconf/raw?ref=${HEAD}" \
	     > $@

## Updating of the configuration; one pattern rule, and one specific
## rule for each of TARGETS, generated dynamically.
config-%:
	$(eval OS=$(word 2,$(subst -, ,$@)))
	$(eval EMACSVERSION=   $(shell grep ^EMACSVERSION        Makeconf-$* | cut -d = -f 2))
	$(eval EMACSPATCHLEVEL=$(shell grep ^EMACSPATCHLEVEL     Makeconf-$* | cut -d = -f 2))
	$(eval DISTVERSION=    $(shell grep ^DISTVERSION         Makeconf-$* | cut -d = -f 2))
	$(eval ESSVERSION=     $(shell grep ^ESSVERSION          Makeconf-$* | cut -d = -f 2))
	$(eval AUCTEXVERSION=  $(shell grep ^AUCTEXVERSION       Makeconf-$* | cut -d = -f 2))
	$(eval POLYMODEVERSION=$(shell grep ^POLYMODEVERSION     Makeconf-$* | cut -d = -f 2))
	$(eval MARKDOWNVERSION=$(shell grep ^MARKDOWNMODEVERSION Makeconf-$* | cut -d = -f 2))
	$(eval TABBARVERSION=  $(shell grep ^TABBARVERSION       Makeconf-$* | cut -d = -f 2))
	$(eval EXECPATHVERSION=$(shell grep ^EXECPATHVERSION     Makeconf-$* | cut -d = -f 2))
	$(eval HUNSPELLVERSION=$(shell grep ^HUNSPELLVERSION     Makeconf-$* | cut -d = -f 2))
	$(eval DICT-ENVERSION= $(shell grep ^DICT-ENVERSION      Makeconf-$* | cut -d = -f 2))
	$(eval DICT-FRVERSION= $(shell grep ^DICT-FRVERSION      Makeconf-$* | cut -d = -f 2))
	$(eval DICT-DEVERSION= $(shell grep ^DICT-DEVERSION      Makeconf-$* | cut -d = -f 2))
	$(eval DICT-ESVERSION= $(shell grep ^DICT-ESVERSION      Makeconf-$* | cut -d = -f 2))
	$(eval VERSION=${EMACSVERSION}$(if ${EMACSPATCHLEVEL},-${EMACSPATCHLEVEL},)-modified-${DISTVERSION})
	$(eval PKGURL=$(shell curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	                           --silent \
	                           ${APIURL}-${OS}/releases/v${VERSION}/assets/links \
	                      | sed -E 's/.*\"direct_asset_url\":\"([^\"]*)\".*/\1/'))
	$(eval NEWSURL=https://gitlab.com/${GROUPNAME}/${REPOSSTEM}-${OS}/-/releases/v${VERSION})
	awk 'BEGIN { FS = "=" } \
	     /version-$*/   { sub(/"[^"]*"/, "\"" "${VERSION}" "\"") } \
	     /installer-$*/ { sub(/"[^"]*"/, "\"" "${PKGURL}" "\"") } \
	     /news-$*/      { sub(/"[^"]*"/, "\"" "${NEWSURL}" "\"") } \
	     /ess-$*/       { sub(/"[^"]*"/, "\"" "${ESSVERSION}" "\"") } \
	     /auctex-$*/    { sub(/"[^"]*"/, "\"" "${AUCTEXVERSION}" "\"") } \
	     /polymode-$*/  { sub(/"[^"]*"/, "\"" "${POLYMODEVERSION}" "\"") } \
	     /markdown-$*/  { sub(/"[^"]*"/, "\"" "${MARKDOWNVERSION}" "\"") } \
	     /tabbar-$*/    { sub(/"[^"]*"/, "\"" "${TABBARVERSION}" "\"") } \
	     /execpath-$*/  { sub(/"[^"]*"/, "\"" "${EXECPATHVERSION}" "\"") } \
	     /hunspell-$*/  { sub(/"[^"]*"/, "\"" "${HUNSPELLVERSION}" "\"") } \
	     /dict-en-$*/   { sub(/"[^"]*"/, "\"" "${DICT-ENVERSION}" "\"") } \
	     /dict-fr-$*/   { sub(/"[^"]*"/, "\"" "${DICT-FRVERSION}" "\"") } \
	     /dict-de-$*/   { sub(/"[^"]*"/, "\"" "${DICT-DEVERSION}" "\"") } \
	     /dict-es-$*/   { sub(/"[^"]*"/, "\"" "${DICT-ESVERSION}" "\"") } \
	     1' \
	    ${CONFIG} > tmpfile && \
	  mv tmpfile ${CONFIG}
	rm -f Makeconf-$*

define config_template
config-$(1): Makeconf-$(1)
endef
$(foreach t,${TARGETS},$(eval $(call config_template,${t})))

commit:
	git add hugo.toml \
	    content/_index.md \
	    content/macos/_index.md \
	    content/windows/_index.md && \
	git commit -m "Update the site for new releases" && \
	git push
